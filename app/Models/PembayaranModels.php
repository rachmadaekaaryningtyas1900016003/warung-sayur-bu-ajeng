<?php

namespace App\Models;

use CodeIgniter\Model;

class PembayaranModels extends Model
{
    protected $table = 'pembayaran';
    protected $allowedFields = ['id', 'booking_id'];
    protected $useTimestamps = true;

    public function getbyPayment()
    {
        return $this->findAll();
    }

    public function addbyPayment($data)
    {
        return $this->save($data);
    }

    public function updatebyPayment($data, $id)
    {
        return $this->update($data, $id);
    }

    public function deletebyPayment($data)
    {
        return $this->delete($data);
    }
}
