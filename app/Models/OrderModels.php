<?php

namespace App\Models;

use CodeIgniter\Model;

class OrderModels extends Model
{
    protected $table = 'order';
    protected $allowedFields = ['id', 'produk_id', 'user_id', 'created_at', 'updated_at'];
    protected $useTimestamps = true;

    public function getOrder()
    {
        return $this->findAll();
    }

    public function addOrder($data)
    {
        return $this->save($data);
    }

    public function updateOrder($data, $id)
    {
        return $this->update($data, $id);
    }

    public function deleteOrder($data)
    {
        return $this->delete($data);
    }
}
