<?php

namespace App\Controllers;

use App\Models\ProdukModels;

class Produk extends BaseController
{
    // membuat variabel produkModel
    protected $produkModel;
    public function __construct()
    {
        // memanggil produk models
        $this->produkModel = new ProdukModels();
    }

    public function index()
    {
        $data = [
            'title' => 'Data Produk',
            'produk' => $this->produkModel->getAllProduk(),
            'validation' => \Config\Services::validation()
        ];
        return view('admin/data-produk', $data);
    }

    public function simpan()
    {
        // validasi input
        if (!$this->validate([
            'id' => [
                'rules' => 'required|is_unique[produk.id]',
                'errors' => [
                    'is_unique' => 'id sudah digunakan',
                    'required' => 'id produk harus diisi'
                ]
            ],
            'nama' => [
                'rules' => 'required|string',
                'errors' => [
                    'string' => 'field nama harus berisi huruf',
                    'required' => 'nama produk harus diisi'
                ]
            ],
            'harga' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'harga produk harus diisi'
                ]
            ],
            'foto' => [
                'rules' => 'uploaded[foto]|max_size[foto,1024]|is_image[foto]|mime_in[foto,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'uploaded' => 'foto produk harus diupload',
                    'max_size' => 'ukuran foto terlalu besar',
                    'is_image' => 'file harus berupa image',
                    'mime_in' => 'file harus berupa jpg, jpeg, png'
                ]
            ],
            'stok_barang' => [
                'rules' => 'required|integer',
                'errors' => [
                    'required' => 'field stok barang harus diisi',
                    'integer' => 'field stok barang harus berisi angka'
                ]
            ]
        ])) {
            // menampilkan pesan ketika error pada field
            session()->setFlashdata('error', $this->validator->listErrors());

            return redirect()->to('admin/data-produk')->withInput();
        }

        // mengambil data foto
        $fotoProduk = $this->request->getFile('foto');

        // menentukan nama secara acak
        $namaProduk = $fotoProduk->getRandomName();

        // memindahkan image ke folder img
        $fotoProduk->move('img', $namaProduk);

        $data = [
            'id' => $this->request->getVar('id'),
            'nama' => $this->request->getVar('nama'),
            'harga' => $this->request->getVar('harga'),
            'foto' => $namaProduk,
            'stok_barang' => $this->request->getVar('stok_barang'),
        ];

        $this->produkModel->addProduk($data);

        // menampilkan pesan ketika data berhasil ditambahkan
        session()->setFlashdata('berhasil', 'Data produk berhasil ditambahkan');

        return redirect()->to('admin/data-produk');
    }

    public function ubah()
    {
        if (!$this->validate([
            // validasi input
            'nama' => [
                'rules' => 'required|string',
                'errors' => [
                    'string' => 'field nama harus berisi huruf',
                    'required' => 'nama produk harus diisi'
                ]
            ],
            'harga' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'harga produk harus diisi'
                ]
            ],
            'foto' => [
                'rules' => 'max_size[foto,1024]|is_image[foto]|mime_in[foto,image/jpg,image/jpeg,image/png]',
                'errors' => [
                    'max_size' => 'ukuran foto terlalu besar',
                    'is_image' => 'file harus berupa image',
                    'mime_in' => 'file harus berupa jpg, jpeg, png'
                ]
            ],
            'stok_barang' => [
                'rules' => 'required|integer',
                'errors' => [
                    'required' => 'field stok barang harus diisi',
                    'integer' => 'field stok barang harus berisi angka'
                ]
            ]
        ])) {
            // menampilkan data ketika terjadi kesalahan
            session()->setFlashdata('error', $this->validator->listErrors());

            return redirect()->to('admin/data-produk')->withInput();
        }

        // mengamil data pada foto
        $fotoProduk = $this->request->getFile('foto');

        // menamai gambar secara acak
        $namaProduk = $fotoProduk->getRandomName();

        // memindahkan gambar ke folder img
        $fotoProduk->move('img', $namaProduk);

        $id = $this->request->getVar('id');

        $data = [
            'nama' => $this->request->getVar('nama'),
            'harga' => $this->request->getVar('harga'),
            'foto' => $namaProduk,
            'stok_barang' => $this->request->getVar('stok_barang')
        ];

        // method untuk update data
        $this->produkModel->updateProduk($id, $data);

        // menampilkan pesan ketika data berhasil di ubah
        session()->setFlashdata('ubah', 'Data produk berhasil diubah');

        return redirect()->to('admin/data-produk');
    }

    public function hapus()
    {
        // mengambil data id
        $data = [
            'id' => $this->request->getVar('id')
        ];

        // method untuk menghapus data
        $this->produkModel->deleteProduk($data);

        // menampilkan pesan ketika data berhasil dihapus
        session()->setFlashdata('hapus', 'Produk berhasil dihapus');

        return redirect()->to('admin/data-produk');
    }
}
