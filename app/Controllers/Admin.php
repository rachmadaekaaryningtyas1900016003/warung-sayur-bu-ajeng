<?php

namespace App\Controllers;


class Admin extends BaseController
{

    public function index()
    {
        $data = [
            'title' => 'Dashboard'
        ];
        return view('admin/index', $data);
    }

    public function home()
    {
        $data = [
            'title' => 'Home admin'
        ];

        return view('admin/home', $data);
    }
}
