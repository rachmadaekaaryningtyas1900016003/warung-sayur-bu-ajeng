<?php

namespace App\Controllers;

use App\Models\ProdukModel;
use App\Models\ProdukModels;

class User extends BaseController
{

    protected $produkModel;
    public function __construct()
    {
        $this->produkModel = new ProdukModels();
    }
    public function index()
    {
        $data = [
            'title' => 'Dashboard User',
            'produk' => $this->produkModel->getAllProduk()
        ];
        return view('user/daftar-produk', $data);
    }
}
