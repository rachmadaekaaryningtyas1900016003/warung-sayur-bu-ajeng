<?php

namespace App\Controllers;

use App\Models\BookingModels;
use App\Models\OrderModels;
use App\Models\ProdukModels;
use Faker\Extension\Helper;

use function PHPUnit\Framework\returnSelf;

class Order extends BaseController
{
    protected $orderModels, $cart, $produkModels;
    public function __construct()
    {
        $this->orderModels = new OrderModels();
        $this->produkModels = new ProdukModels();
        $this->cart = \Config\Services::cart();
    }

    public function index()
    {
        $data = [
            'title' => 'Daftar Produk',
            'produk' => $this->produkModels->getAllProduk(),
            'validation' => \Config\Services::validation(),

        ];
        return view('user/daftar-produk', $data);
    }

    public function cekOrder()
    {
        # code...
        $response = $this->cart->contents();
        $data = json_encode($response);

        echo '<pre>';
        print_r($response);
        echo '</pre>';
    }

    public function addOrder()
    {
        $data = [
            'id'      => $this->request->getVar('id'),
            'qty'     => 1,
            'price'   => $this->request->getVar('harga'),
            'name'    => $this->request->getVar('nama'),
            'options' => [
                'foto' => $this->request->getVar('foto'),
            ]
        ];

        $this->cart->insert($data);

        session()->setFlashdata('berhasil', 'Selamat item berhasil ditambahkan!');

        return redirect()->to('user/daftar-produk');
    }

    public function getOrder()
    {
        # code...
        $data = [
            'title' => 'Detail Pesanan',
            'cart' => \Config\Services::cart(),
        ];

        return view('user/detail-pesanan', $data);
    }

    public function updateOrder()
    {
        $i = 1;
        foreach ($this->cart->contents() as $value) {
            # code...
            $this->cart->update(array(
                'rowid'   => $value['rowid'],
                'qty'     => $this->request->getVar('qty' . $i++),
            ));
        }

        session()->setFlashdata('berhasil', 'Selamat keranjang berhasil diperbarui');
        return redirect()->to('user/detail-pesanan');
    }

    public function deleteOrder($rowid)
    {
        $this->cart->remove($rowid);

        session()->setFlashdata('berhasil', 'Produk berhasil dihapus!');
        return redirect()->to('user/detail-pesanan');
    }

    public function checkout()
    {
    }
}
