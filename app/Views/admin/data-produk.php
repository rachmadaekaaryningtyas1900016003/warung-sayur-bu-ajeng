<?php $this->extend('templates/index'); ?>

<?php $this->section('page-content'); ?>

<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Produk</h6>
        </div>
        <div class="card-body">
            <button type="button" class="btn btn-success mb-3 mr-3" data-bs-toggle="modal" data-bs-target="#modalTambah">
                Tambah Data
            </button>
            <?php if (session()->getFlashdata('berhasil')) : ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->getFlashdata('berhasil'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <?php if (session()->getFlashdata('gagal')) : ?>
                <div class="alert alert-warning pb-0" role="alert">
                    <?= session()->getFlashdata('error'); ?>
                </div>
            <?php endif; ?>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width: 10px;">No</th>
                            <th style="width: 50px;">Kode Produk</th>
                            <th>Nama Produk</th>
                            <th>Harga Produk</th>
                            <th style="width: 20px;">Stok Barang</th>
                            <th style="width: 30px;">Status</th>
                            <th style="width: fit-content; justify-content: center; align-items: center;">Foto</th>
                            <th style="width: 130px;">Action</th>
                        </tr>
                    </thead>
                    <?php $i = 1 ?>
                    <?php foreach ($produk as $produk) : ?>
                        <tbody>
                            <tr>
                                <td><?= $i++; ?></td>
                                <td><?= $produk['id']; ?></td>
                                <td><?= $produk['nama']; ?></td>
                                <td><?= number_to_currency($produk['harga'], 'Rp. ') ?></td>
                                <td><?= $produk['stok_barang']; ?></td>
                                <td><?php if ($produk['stok_barang'] > 0) { ?>
                                        <a style="text-decoration: none;" class=" p-1 rounded btn-success">Tersedia</a>
                                    <?php } else { ?>
                                        <a style="text-decoration: none;" class=" p-1 rounded btn-danger">Terjual</a>
                                    <?php } ?>
                                </td>
                                <td style="width: 250px; padding: 1rem;">
                                    <img style="width: 220px;  height: 150px; object-fit: cover; border-radius: 10%;" src="/img/<?= $produk['foto']; ?>" alt="<?= $produk['nama']; ?>">
                                </td>
                                <td>
                                    <button type="button" id="btn-edit-produk" class="btn btn-warning mx-1" data-toggle="modal" data-target="#modalEdit" data-id="<?= $produk['id']; ?>" data-nama="<?= $produk['nama']; ?>" data-stok_barang="<?= $produk['stok_barang']; ?>" data-harga="<?= $produk['harga']; ?>">
                                        <i class="far fa-edit"></i></button>
                                    <button type="button" id="btn-hapus-produk" class="btn btn-danger mx-1" data-toggle="modal" data-target="#modalHapus" data-id="<?= $produk['id']; ?>">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- Modal tambah data produk-->
<div class="modal fade" id="modalTambah" tabindex="-1" aria-labelledby="modalTambah" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content p-2">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Produk</h5>
            </div>
            <div class="modal-body">
                <form action="/Produk/simpan" method="post" enctype="multipart/form-data" class="row g-3">
                    <?= csrf_field(); ?>
                    <div>
                        <label for="inputtext4" class="form-label">Kode Produk</label>
                        <input type="text" class="form-control" id="id" name="id">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Foto</label>
                        <input type="file" class="form-control" id="foto" name="foto">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Nama Produk</label>
                        <input type="text" class="form-control" id="nama" name="nama">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Stok Barang</label>
                        <input type="text" class="form-control" id="stok_barang" name="stok_barang">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Harga Produk</label>
                        <input type="text" class="form-control" id="harga" name="harga">
                    </div>

                    <div class="col-12 pt-2 form-group gap-3 d-flex justify-content-end">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal edit data produk-->
<div class="modal fade" id="modalEdit">
    <div class="modal-dialog">
        <div class="modal-content p-2">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data Produk</h5>
            </div>
            <div class="modal-body">
                <form action="/Produk/ubah" method="post" enctype="multipart/form-data" class="row g-3">
                    <?= csrf_field(); ?>
                    <div>
                        <input type="hidden" class="form-control" id="id" name="id">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Foto</label>
                        <input type="file" class="form-control" id="foto" name="foto">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Stok Barang</label>
                        <input type="text" class="form-control" id="stok_barang" name="stok_barang">
                    </div>
                    <div>
                        <label for="inputtext4" class="form-label">Harga Produk</label>
                        <input type="text" class="form-control" id="harga" name="harga">
                    </div>

                    <div class="col-12 pt-2 form-group gap-3 d-flex justify-content-end">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal hapus data produk-->
<div class="modal fade" id="modalHapus">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/Produk/hapus/<?= $produk['id']; ?>" method="post">
                <div class="modal-body">
                    Apakah anda yakin menghapus data ini?
                    <input type="hidden" name="id" id="id">
                </div>
                <div class="col-12 pt-2 form-group gap-3 d-flex justify-content-end">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->endSection(); ?>