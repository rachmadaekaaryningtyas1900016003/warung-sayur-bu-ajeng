<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
            <img src="<?= base_url(); ?>/img/logo.jpg" width="50" height="50" alt="">
        </div>
        <div class="sidebar-brand-text mx-3">Warung Sayur</div>
    </a>

    <!-- Divider Admin -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <?php if (in_groups('admin')) : ?>
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('admin'); ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
    <?php endif; ?>


    <!-- Divider User -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Menu
    </div>

    <?php if (in_groups('user')) : ?>
        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('user/daftar-produk'); ?>">
                <i class="fas fa-list-ul"></i>
                <span>Katalog Produk</span></a>
        </li>
    <?php endif; ?>

    <?php if (in_groups('admin')) : ?>
        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('admin/data-produk'); ?>">
                <i class="fas fa-fw fa-table"></i>
                <span>Data Produk</span></a>
        </li>

        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('admin/data-transaksi'); ?>">
                <i class="fas fa-fw fa-users"></i>
                <span>Data Transaksi</span></a>
        </li>
    <?php endif; ?>

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>