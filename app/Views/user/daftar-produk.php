<?= $this->extend('templates/index'); ?>

<?= $this->section('page-content'); ?>

<div class="container-fluid">

    <?php if (session()->getFlashdata('berhasil')) : ?>
        <div class="alert alert-success" role="alert">
            <?= session()->getFlashdata('berhasil'); ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>

    <div class="d-flex justify-content-center flex-lg-wrap">
        <?php foreach ($produk as $prd) : ?>
            <?php
            echo form_open('Order/addOrder');
            echo form_hidden('id', $prd['id']);
            echo form_hidden('foto', $prd['foto']);
            echo form_hidden('nama', $prd['nama']);
            echo form_hidden('harga', $prd['harga']);
            ?>
            <div class="card m-3 shadow " style="width: 18rem;">
                <img class="card-img-top" height="200" src="/img/<?= $prd['foto']; ?>" alt="<?= $prd['nama']; ?>">
                <div class="card-body">
                    <h5 class="card-title"><b><?= $prd['nama']; ?></b></h5>
                    <p class="card-text">Rp. <?= $prd['harga']; ?></p>
                    <button type="submit" class="btn btn-primary">Beli</button>
                </div>
            </div>
            <?php echo form_close() ?>
        <?php endforeach; ?>
    </div>

</div>

<?= $this->endsection(); ?>