<?= $this->extend('templates/index'); ?>


<?= $this->section('page-content'); ?>

<div class="container mt-5 mb-5 shadow">
    <div class="d-flex justify-content-center row">

        <div class="col-md-12 py-4">
            <?php if (session()->getFlashdata('berhasil')) : ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->getFlashdata('berhasil'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>

            <div class="receipt bg-white p-3 rounded"><img src="<?= base_url(); ?>/img/logo.jpg" width="120">
                <h4 class="mt-2 mb-3">Keranjang Belanja Saya</h4>
                <h6 class="name">Hello, <?= user()->username; ?> kamu sudah belanja apa hari ini?</h6>
                <hr>

                <div class="d-flex flex-row justify-content-between align-items-center order-details">
                    <div><span class="d-block fs-12">Tanggal Order</span><span class="font-weight-bold"><?= date('d F Y'); ?></span></div>
                    <div><span class="d-block fs-12">ID Order</span><span class="font-weight-bold">9394827349</span></div>
                    <div><span class="d-block fs-12">Pembayaran</span><span class="font-weight-bold">Credit card</span><img class="ml-1 mb-1" src="https://i.imgur.com/ZZr3Yqj.png" width="20"></div>
                    <div><span class="d-block fs-12">Lokasi Tujuan</span><span class="font-weight-bold text-success">Yogyakarta</span></div>
                </div>

                <hr>
                <?php echo form_open('Order/updateOrder'); ?>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th width="100px">Qty</th>
                            <th width="90px">Gambar</th>
                            <th width="60%">Nama Barang</th>
                            <th>Harga</th>
                            <th width="70px">Action</th>
                        </tr>
                    </thead>
                    <?php
                    $i = 1;
                    foreach ($cart->contents() as $keranjang) :
                        $tax = $cart->total() * 10 / 100;

                        $shipping = $cart->total();
                        if ($shipping > 30000) {
                            $ongkir = $shipping * 25 / 100;
                        } else {
                            $ongkir = $shipping * 15 / 100;
                        }

                        $total = $cart->total() + $tax + $ongkir;
                    ?>
                        <tbody>
                            <tr>
                                <td> <input style="width: 50px;" type="number" min="1" name="qty<?= $i++; ?>" class="form-contro" value="<?= $keranjang['qty']; ?>"></td>
                                <td><img class="rounded" src="/img/<?= $keranjang['options']['foto']; ?>" width="80" height="80"></td>
                                <td><?= $keranjang['name']; ?></td>
                                <td><?= number_to_currency($keranjang['subtotal'], 'Rp. ') ?></td>
                                <td><a href="<?= base_url('Order/deleteOrder/' . $keranjang['rowid']); ?>" class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></a></td>
                            </tr>
                        </tbody>
                    <?php endforeach; ?>
                </table>

                <div class="mt-5 amount row">
                    <div class="d-flex justify-content-center col-md-6"><img src="https://i.imgur.com/AXdWCWr.gif" width="250" height="100"></div>
                    <div class="col-md-6">
                        <div class="billing">
                            <div class="d-flex justify-content-between"><span>Subtotal</span><span class="font-weight-bold"><?= number_to_currency($cart->total(), 'Rp. ') ?></span></div>
                            <div class="d-flex justify-content-between mt-2"><span>Shipping fee</span><span class="font-weight-bold">
                                    <?= number_to_currency($ongkir, 'Rp. '); ?>
                                </span></div>
                            <div class="d-flex justify-content-between mt-2"><span>Tax</span><span class="font-weight-bold"><?= number_to_currency($tax, 'Rp. ') ?></span></div>
                            <hr>
                            <div class="d-flex justify-content-between mt-1"><span class="font-weight-bold">Total</span><span class="font-weight-bold text-success"><?= number_to_currency($total, 'Rp. ') ?></span></div>
                        </div>
                    </div>
                </div>

                <div class="d-flex mt-5 mb-2 justify-content-between">
                    <button class="btn btn-warning" type="submit"><i class="far fa-save"></i> Update</button>
                    <a class="btn btn-primary mr-2" href="<?= base_url('user/daftar-produk'); ?>"><i class="far fa-credit-card"></i> Checkout</a>
                </div>

                <?php echo form_close(); ?>
                <hr>

                <div class="d-flex justify-content-between align-items-center footer">
                    <div class="thanks"><span class="d-block font-weight-bold">Terima kasih telah berbelanja</span><span>Warung Sayur team</span></div>
                    <div class="d-flex flex-column justify-content-end align-items-end"><span class="d-block font-weight-bold">Need Help?</span><span>+62 123-3231-5830</span></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>